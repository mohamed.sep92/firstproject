"use strict";
/*
An object is a special type of data that contains within itself other values ​​that can be changed, 
added and removed, as well as functions for working with these values,
Store in one variable all the data about some semantic unit,
Store in one place not only data, but also functions that will work with this data. 
*/

//option 1
function createNewUser() {
    return {
        "first Name": prompt("Enter your first name: "),
        "last Name": prompt("Enter your last name: "),
    
        getLogin: function() {
            return (this["first Name"][0] + this["last Name"]).toLowerCase();
        }
    }
}
let user = createNewUser();
console.log(user);
console.log(user.getLogin());


/*
//option 2
let createNewUser = function() {
    
    let newUser = {
        "first Name": prompt("Enter your first name: "),
        "last Name": prompt("Enter your last name: "),

        getLogin: function() {
            return (this["first Name"][0] + this["last Name"]).toLowerCase();
        }
    };
    return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
*/




/*
//option 3
let createNewUser = function(newUser) {
    
    newUser = {
        "first Name": prompt("Enter your first name: "),
        "last Name": prompt("Enter your last name: "),

        getLogin: function() {
            return (this["first Name"][0] + this["last Name"]).toLowerCase();
        }
    };
    return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
*/