"use strict";
/*
A JavaScript function is a block of code designed to perform a particular task,
Functions are the basic "building blocks" of a program,
functions are needed in programming in order not to repeat the same code in many places.
*/

/*
Arguments are Passed by Value
The parameters, in a function call, are the function's arguments,
JavaScript arguments are passed by value: 
The function only gets to know the values, not the argument's locations,
If a function changes an argument's value, 
it does not change the parameter's original value.
*/


// task option 1
let operator = prompt('Enter operator to perform the calculation ( either +, -, * or / ): ');
console.log("the operator: " + operator);  
let number1 = +(prompt ('Enter the first number: '));  
console.log("the first number: " + number1);
let number2 = +(prompt ('Enter the second number: ')); 
console.log("the second number: " + number2);

function calc(number1, number2) {
    if (operator == '+') {
        return number1 + number2;  
    }
    
    else if (operator == '-') {
        return number1 - number2;
    }
    
    else if (operator == '*') {
        return number1 * number2;  
    }
    else if (operator == '/') {  
        return number1 / number2;
    } else {
        return "error";
    }
}

console.log(" Result is " + calc(number1, number2));      


/*
//option 2
let x;
let y;

x = +prompt("Enter number 1", '');
console.log(x);
y = +prompt("Enter number 2", '');
console.log(y);

function calculator(x, y, o) {
    switch (o) {
        case '+':
            return x + y;
        
        case '-':
            return x - y;
        
        case '*' :
            return x * y;
            
        case '/' :
            return x / y;
    }
}

let cal_1 = calculator(x, y, '+');
let cal_2 = calculator(x, y, '-');
let cal_3 = calculator(x, y, '*');
let cal_4 = calculator(x, y, '/');
console.log(cal_1);
console.log(cal_2);
console.log(cal_3);
console.log(cal_4);
*/