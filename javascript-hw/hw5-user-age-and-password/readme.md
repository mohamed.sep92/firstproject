## Теоретический вопрос

1. Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования

## Задание

Дополнить функцию `createNewUser()` методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

#### Технические требования:
- Возьмите выполненное домашнее задание номер 4 (созданная вами функция `createNewUser()`) и дополните ее следующим функционалом:
   1. При вызове функция должна спросить у вызывающего дату рождения (текст в формате `dd.mm.yyyy`) и сохранить ее в поле `birthday`.
   2. Создать метод `getAge()` который будет возвращать сколько пользователю лет.
   3. Создать метод `getPassword()`, который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
- Вывести в консоль результат работы функции `createNewUser()`, а также функций `getAge()` и `getPassword()` созданного объекта.

#### Литература:
- [Дата и время](https://learn.javascript.ru/datetime)

## Theoretical question

1. Describe in your own words what escaping is and why it is needed in programming languages

## Exercise

Supplement the `createNewUser()` function with methods for calculating the user's age and his password. The task must be implemented in javascript, without the use of frameworks and a supporter of libraries (such as Jquery).

#### Technical requirements:
- Take completed homework number 4 (the `createNewUser()` function you created) and add the following functionality to it:
   1. When called, the function should ask the caller for the date of birth (text in the format `dd.mm.yyyy`) and store it in the `birthday` field.
   2. Create a `getAge()` method that will return how old the user is.
   3. Create a `getPassword()` method that will return the first letter of the user's name in upper case, concatenated with the last name (in lower case) and year of birth. (for example, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
- Print to the console the result of the `createNewUser()` function, as well as the `getAge()` and `getPassword()` functions of the created object.

#### Literature:
- [Date and time](https://learn.javascript.ru/datetime)