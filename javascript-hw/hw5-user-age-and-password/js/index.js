"use strict";
/*
The deprecated escape() method computes a new string in which 
certain characters have been replaced by a hexadecimal escape 
sequence. Use encodeURI or encodeURIComponent instead.
*/

let createNewUser = function() {
  
  let newUser = {

    firstName: prompt("Enter your first name: "),
    lastName: prompt("Enter your last name: "),
    birthDay: prompt("Please enter your date of birth: ", "dd.mm.yyyy"),

    getLogin(){
      return ((this.firstName[0] + this.lastName).toLowerCase());
    },

    getAge(){
      let year_born = this.birthDay.slice(6,10);
      let ageInMilliseconds = Date.now() - new Date(year_born);
      const minute = 1000 * 60;
      const hour = minute * 60;
      const day = hour * 24;
      const year = day * 365;

      let years = Math.round(ageInMilliseconds / year);
      
      return ("Hello, " + "you are " + years + " years old!");
    },
  
    getPassword(){ 
      return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDay.substr(6, 4));
      //return ((this.firstName[0].toUpperCase()) + (this.lastName.toLowerCase()) + (this.birthDay.slice(6,10)));
      //return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDay.substring(6, 10));
    }
  };
  return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());