/*
Loops can execute a block of code a number of times,
to run the same code over and over again, each time with a different value.
JavaScript supports different kinds of loops:
for - loops through a block of code a number of times
for/in - loops through the properties of an object
for/of - loops through the values of an iterable object
while - loops through a block of code while a specified condition is true
do/while - also loops through a block of code while a specified condition is true,
           The do while loop is a variant of the while loop, 
           This loop will execute the code block once, 
           before checking if the condition is true, 
           then it will repeat the loop as long as the condition is true.
*/

"use strict";
//without limit for a user to enter a number

let number = +prompt("Please, enter a number!", '');

while (number == "" || number == null) {
    number = +prompt("Please, enter a number!", '');
    console.log("Sorry, no numbers");
}

console.log(number);

for (let i = 0; i <= number; i += 5) {
    if (true) {
        console.log(i);
    }
}

//if set limit for a user to enter a number 
/*
let number = +prompt("Please, enter a number!", '');
console.log(number);

while (!number) { //advance task check number
    number = +prompt("Please, enter a number!", '');
    console.log(number);
}

for (let i = 0; i <= number; i += 5) {
    if(number<=1000){
        console.log(i);
    } 
}    
while (number>1000) {
    console.log("Sorry, no numbers");
    break;
}
*/

//another solution for hw 2 with set limit for a user to enter a number
/*
let number = +prompt("Please, enter a number!", '');
console.log(number);

while (number == "" || number == null) { //advance task check number
    number = +prompt("Please, enter a number!", '');
    console.log(number);
}

for(let i=0; i<=number; i++) {
    if(i % 5 === 0 && number<=1000 && number>=5){
        console.log(i);
    } else if (number>1000 || number<5) {
        console.log("Sorry, no numbers");
        break;
    }
}
*/
