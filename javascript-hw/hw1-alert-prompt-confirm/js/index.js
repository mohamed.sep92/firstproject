/* 1-
In JavaScript, var, let, and const are three ways of creating variables,

Var
Variables declared using var keyword scoped to the current execution context. 
This means If they are inside a function we only can access them inside the function. 
and If they are not they are part of the global scope which we can access anywhere.

Let
let creates variables that are block-scoped. Also, they can not be redeclared and can be updated.
use let to declare variables if it is not a constant.

Const
Const variables are cannot be updated or redeclared. 
This way is used to declare constants. 
Same as the let declarations const declarations are block-scoped. 
Unlike var and let, If we are using const to declare a variable that must be initialized.
If we use const when object creating we can still update properties inside that object. 
use const to declare constant variables.
*/

/* 2-
One of the issues with using the var keyword is they can be redeclared inside the same scope. 
This will brings up some serious issues if we declare another variable using the same name inside the same scope, 
the new variable will replace the old one. var can be updated as well.
And another issue with var is these variables are not block-scoped which means if we have conditions statements those are not scoped to that statement but to the entire function or to the global scope.
*/



"use strict";

let userName;
userName = prompt("Please,type your name!", '');
console.log(userName, typeof (String(userName)));
let userAge; 
userAge = +prompt("Please, type your age!", '');
console.log(userAge, typeof userAge);

if (userAge < 18) {
    console.log("age less 18");
    alert("You are not allowed to visit this website");
} else if (userAge > 22) {
    console.log("age more 22");
    alert("Welcome, " + userName);
} else if (userAge <= 22 && userAge >=18) {
    console.log("age from 18 to 22");
    if (confirm("Are you sure you want to continue?") == true) {
        console.log("user pressed OK!");
        alert("Welcome, " + userName);
    } else {
        console.log("user pressed cancel!");
        alert("You are not allowed to visit this website");
    }
}





/*
//advance task 

let userName;
userName = prompt("Please,type your name!", '');
console.log(userName, typeof (String(userName)));
let userAge; 
userAge = +prompt("Please, type your age!", '');
console.log(userAge, typeof userAge);

//to check (name-prompt) if user enter number or didn't type at all by click on ok or cancel

let checkingUsername;
if (!(userName && isNaN(userName))) {
    console.log("not valid name");
    checkingUsername = prompt("Please, type your name in letters!", '');
    console.log(checkingUsername);
}

//to check (age-prompt) if user enter letters or didn't type at all by click on ok or cancel

let checkingUserage;
else if (!(userAge && !isNaN(userAge))) { 
    console.log("not valid number");
    checkingUserage = +prompt("Please, type your age in numbers!", '');
    console.log(checkingUserage);
}
*/

//or to check (age-prompt) if user enter letters or didn't type at all by click on ok or cancel

/*
let checkingUserage;
else if (!number) { 
    console.log("not valid number");
    checkingUserage = +prompt("Please, type your age in numbers!", '');
    console.log(checkingUserage);
}
*/

//or to check (age-prompt & name-prompt)
/*
while ((userName == "" || userName == null || userName == Number) || (userAge == !Number)) {
    let checkingUsername = prompt("Please,type your name in letters!", '');
    console.log(checkingUsername, typeof (String(checkingUsername)));
    
    let checkingUserage = prompt("Please,type your age in numbers!", '');
    console.log(checkingUserage, typeof (String(checkingUserage)));
    
}
*/




