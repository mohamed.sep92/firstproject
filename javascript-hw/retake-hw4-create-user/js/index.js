"use strict";
/*
An object is a special type of data that contains within itself other values ​​that can be changed, 
added and removed, as well as functions for working with these values,
Store in one variable all the data about some semantic unit,
Store in one place not only data, but also functions that will work with this data. 
*/

let createNewUser = function() {
    
    let newUser = {
        firstName: prompt("Enter your first name: "),
        lastName: prompt("Enter your last name: "),

        getLogin(){
            return ((this.firstName[0] + this.lastName).toLowerCase());
        }
    };
    return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
