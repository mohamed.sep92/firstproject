"use strict";

/*
An event can be assigned a handler, that is, a function that will work as soon as the event occurs.
handlers that JavaScript code can respond to user actions.There are several ways to assign an event handler.
Event handling is now separated by method, making code easier to maintain.
*/


let x = document.getElementById("myForm");
x.addEventListener("focus", myFocusFunction, true);
x.addEventListener("blur", myBlurFunction, true);


function myFocusFunction() {
    document.getElementById("myInput").style.outline = "none"; 
    document.getElementById("myInput").style.borderColor = "green";
    document.getElementById('myForm').reset();
}

function myBlurFunction() {
    document.getElementById("myInput").style.outline = "";  
    document.getElementById("myInput").style.borderColor = "";
    document.getElementById("myInput").style.color = "";
    
    
    let y = document.getElementById("myInput").value;
    let text;
    if (y < 0 || y == "") {
        text = "Please, Enter correct price!";
        document.getElementById("test").innerHTML = text;
        document.getElementById("myInput").style.borderColor = "red";
        
    } else if (y > 0){
        text = "Input Ok!";
        document.getElementById("test").innerHTML = text;
        document.getElementById("myInput").style.color = "green";


        //closable span: to works multiple time we need to refresh the page every time when enter a correct number
        let panes = document.querySelectorAll('.span');
        for(let span of panes) {
            span.insertAdjacentHTML("beforeend", "current price: $" + y);
            span.insertAdjacentHTML("beforeend", '<button class="remove-button">&times</button>');
            span.onclick = () => span.remove();
        }
    }
}


/* //option
let x = document.getElementById("myForm");
x.addEventListener("focus", myFocusFunction, true);
x.addEventListener("blur", myBlurFunction, true);


function myFocusFunction() {
    document.getElementById("myInput").style.outline = "none"; 
    document.getElementById("myInput").style.borderColor = "green";
    document.getElementById('myForm').reset();
}

function myBlurFunction() {
    document.getElementById("myInput").style.outline = "";  
    document.getElementById("myInput").style.borderColor = "";
    document.getElementById("myInput").style.color = "";
    
    
    let y = document.getElementById("myInput").value;
    let text;
    if (y < 0 || y == "") {
        text = "Please, Enter correct price!";
        document.getElementById("test").innerHTML = text;
        document.getElementById("myInput").style.borderColor = "red";
        
    } else if (y > 0){
        text = "Input Ok!";
        document.getElementById("test").innerHTML = text;
        document.getElementById("myInput").style.color = "green";
        
        let panes = document.querySelectorAll('.span');
        let i;
        for(i = 0; i < panes.length; i++) {
            panes[i].insertAdjacentHTML("beforeend", "current price: $" + y);
            panes[i].insertAdjacentHTML("beforeend", '<button class="remove-button">&times</button>');

            //reset input field put without closable span, so (no need) to refresh the page every time when enter a correct number
            panes[i].onclick = () => 'none';

            //closable span: to works multiple time we need to refresh the page every time when enter correct number
            // panes[i].addEventListener("click", function() {
            //     this.style.display = 'none';
            // });
        }
    }
} */





