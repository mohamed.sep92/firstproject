"use strict";

/*
With the HTML DOM, JavaScript can access and change all the elements of an HTML document.
The HTML DOM (Document Object Model)is constructed as a tree of Objects
When a web page is loaded, the browser creates a Document Object Model of the page.
*/


let data = ["1", "2", "3", "sea", "user", 23];

let myList = document.querySelector('#myList');

myList.innerHTML = '<ul>' + data.map(function (data) {
	return '<li>' + data + '</li>';
}).join('') + '</ul>';




/* 
//option 1 without method map

let data = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let list = document.getElementById("myList");

data.forEach((item) => {
    let li = document.createElement("li");
    li.innerText = item;
    list.appendChild(li);
});
*/


/*
//option 2 without method map
function makeList() {
    let listData = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"],
    listContainer = document.createElement('div'),
    listElement = document.createElement('ul'),
    numberOfListItems = listData.length,
    listItem,
    i;
    document.getElementsByTagName('body')[0].appendChild(listContainer);
    listContainer.appendChild(listElement);
    for (i = 0; i < numberOfListItems; ++i) {
        listItem = document.createElement('li');
        listItem.innerHTML = listData[i];
        listElement.appendChild(listItem);
    }
}
makeList();
*/












