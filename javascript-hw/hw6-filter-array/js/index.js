"use strict";
/*
The forEach() method executes a provided function once for each array element.
To iterate over elements forEach(func) calls func for each element. Returns nothing.
*/

let arr = ['hello', 'world', 23, '23', null];
let dataType = 'number';
let filterBy = arr.filter((arr, dataType) => typeof arr !== typeof dataType);
console.log( filterBy );

//option
/* let arr = ['hello', 'world', 23, '23', null];
let filterBy = arr.filter(item => typeof item !== 'number');
console.log( filterBy ); */